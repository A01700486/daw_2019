<?php 
  session_start();
  require_once("model.php"); 

  $titulo = "Buscador";
  include("_header.html");  

  if(isset($_SESSION["ver"])) {
      include("_form.html"); 

  }

  if(isset($_SESSION["registrar"])) {
      include("_btn_agregar.html");
      
  }

  if(isset($_SESSION["ver"])) {
      if (isset($_POST["lugar"])) {
          $lugar = htmlspecialchars($_POST["lugar"]);
      } else {
          $lugar = "";
      }

     if (isset($_POST["estado"])) {
          $estado = htmlspecialchars($_POST["estado"]);
      } else {
          $estado = "";
      }

      echo '<div id="resultados_consulta">';
      echo consultar_casos($lugar,$estado);
      echo '</div>';
  }

  if(isset($_SESSION["registrar"])) {
    include("_btn_agregar.html");
  }
  include("_preguntas.html");

  include("_footer.html"); 
?>