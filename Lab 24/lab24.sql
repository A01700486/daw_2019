-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 03-06-2020 a las 01:09:45
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `lab23`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `REGISTRAR_DEPOSITO_VENTANILLA` (`eNoCuenta` VARCHAR(12), `emonto` NUMERIC(8,2))  BEGIN
	START TRANSACTION;
		INSERT INTO MOVIMIENTOS VALUES(eNoCuenta, 'B', NOW(), emonto);
        UPDATE CLIENTES_BANCA SET Saldo = Saldo + emonto WHERE NoCuenta = eNoCuenta;
        Commit;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `REGISTRAR_RETIRO_CAJERO` (`eNoCuenta` VARCHAR(12), `emonto` NUMERIC(8,2))  BEGIN
	START TRANSACTION;
		INSERT INTO MOVIMIENTOS VALUES(eNoCuenta, 'A', NOW(), emonto);
        UPDATE CLIENTES_BANCA SET Saldo = Saldo - emonto WHERE NoCuenta = eNoCuenta;
        Commit;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes_banca`
--

CREATE TABLE `clientes_banca` (
  `NoCuenta` varchar(5) NOT NULL,
  `Nombre` varchar(30) NOT NULL,
  `Saldo` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes_banca`
--

INSERT INTO `clientes_banca` (`NoCuenta`, `Nombre`, `Saldo`) VALUES
('001', 'Manuel Rios Maldonado', 8500),
('002', 'Pablo Perez Ortiz', 5000),
('003', 'Luis Flores Alvarado', 8000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `NoCuenta` varchar(5) NOT NULL,
  `ClaveM` varchar(2) NOT NULL,
  `Monto` int(10) NOT NULL,
  `Fecha` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`NoCuenta`, `ClaveM`, `Monto`, `Fecha`) VALUES
('001', 'A', 500, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipos_movimiento`
--

CREATE TABLE `tipos_movimiento` (
  `ClaveM` varchar(2) NOT NULL,
  `Descripcion` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipos_movimiento`
--

INSERT INTO `tipos_movimiento` (`ClaveM`, `Descripcion`) VALUES
('A', 'Retiro Cajero Automatico'),
('B', 'Deposito Ventanilla');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes_banca`
--
ALTER TABLE `clientes_banca`
  ADD PRIMARY KEY (`NoCuenta`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`NoCuenta`,`ClaveM`);

--
-- Indices de la tabla `tipos_movimiento`
--
ALTER TABLE `tipos_movimiento`
  ADD PRIMARY KEY (`ClaveM`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
