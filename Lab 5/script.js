// Problema 1

function confirmacion(){
  let str1 = document.getElementById("contraseña").value;
  let str2 = document.getElementById("confirma").value;
  if(str1==str2){
    document.getElementById("demo").innerHTML = "Contraseña validada" ;
  }else{
    document.getElementById("demo").innerHTML = "Lo sentimos, las contraseñas no coinciden";
  }
}

document.getElementById("boton_confirma").onclick = confirmacion ;

//Problema 2
function tienda(){
  let precio_gorras = 150;
  let precio_chanclas = 50;
  let precio_gafas = 60;
  let num_gorras = document.getElementById("gorras").value;
  let num_chanclas = document.getElementById("chanclas").value;
  let num_gafas = document.getElementById("lentes").value;
  let costo = 0;
  costo += num_chanclas*precio_chanclas;
  costo += num_gorras*precio_gorras;
  costo += num_gafas*precio_gafas;
  document.getElementById("costo_Total").innerHTML = "Tu compra consta de: <br>" + num_chanclas + " chanclas <br>" + num_gafas + " lentes de sol<br>" + num_gorras + " gorras<br> El costo total de tu pedido es de: $" + costo;
}

document.getElementById("boton_tienda").onclick = tienda;

//Problema 3

function test(){
  let puntosBombon = 0;
  let puntosBurbuja = 0;
  let puntosBellota = 0;
  let puntosMojo = 0;
  if(document.getElementById("color").value == 1){
    puntosBombon++;
  }else  if(document.getElementById("color").value == 2){
    puntosBurbuja++;
  }else if (document.getElementById("color").value == 3){
    puntosBellota++;
  }else{
    puntosMojo++;
  }

  if(document.getElementById("monstruo").value == 1){
    puntosBombon++;
  }else  if(document.getElementById("monstruo").value == 2){
    puntosBurbuja++;
  }else if (document.getElementById("monstruo").value == 3){
    puntosBellota++;
  }else{
    puntosMojo++;
  }

  if(document.getElementById("palabra").value == 1){
    puntosBombon++;
  }else  if(document.getElementById("palabra").value == 2){
    puntosBurbuja++;
  }else if (document.getElementById("palabra").value == 3){
    puntosBellota++;
  }else{
    puntosMojo++;
  }
  
  if(puntosBombon>=2){
    document.write("Eres Bombon!");
  }else if(puntosBellota>=2){
    document.write("Eres Bellota!");
  }else if(puntosBurbuja>=2){
    document.write("Eres Burbuja!");
  }else{
    document.write("Eres Mojo Jojo");
  }

}

document.getElementById("listo").onclick = test;