<?php
     function conectar_bd() {
      $conexion_bd = mysqli_connect("localhost","root","","examenparcial");
      if ($conexion_bd == NULL) {
          die("No se pudo conectar con la base de datos");
      }
      return $conexion_bd;
  }
  //función para desconectarse de una bd
  //@param $conexion: Conexión de la bd que se va a cerrar
  function desconectar_bd($conexion_bd) {
      mysqli_close($conexion_bd);
  }

  function crear_select($id, $columna_descripcion, $tabla) {
        $conexion_bd = conectar_bd();  
        $resultado = '<select name="'.$tabla.'" id="'.$tabla.'"><option value="NA" disabled selected>Selecciona una opción</option>';       
        $consulta = "SELECT $id, $columna_descripcion FROM $tabla";
        $resultados = $conexion_bd->query($consulta);
        while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
            $resultado .= '<option value="'.$row["$id"].'" ';
            $resultado .= '>'.$row["$columna_descripcion"].'</option>';
        }
            
        desconectar_bd($conexion_bd);
        $resultado .=  '</select>';
        return $resultado;
      }

   function consultar_casos($lugar="", $estado="",$fecha="") {
    $conexion_bd = conectar_bd();  
    
    $resultado =  "<table><thead><tr><th>Caso</th><th>Lugar</th><th>Estado actual</th><th>Fecha y hora</th></tr></thead>";
    
    $consulta = 'Select LT.id as LT_id, L.nombre_lugar as L_nombre, T.nombreinc as T_nombre, LT.Fecha as LT_Fecha From lugar_tipo as LT, tipo_incidente T, lugar_incidente as L WHERE LT.tipo = T.Id AND LT.lugar = L.id ORDER BY LT.Fecha DESC';
    if ($lugar != "") {
        $consulta .= " AND lugar_id=".$lugar;
    }
    if ($estado != "") {
        $consulta .= " AND estado_id=".$estado;
    }
    if ($fecha != "") {
        $consulta .= " AND caso_id=".$fecha;
    }
      
    $resultados = $conexion_bd->query($consulta);  
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= "<tr>";
        $resultado .= "<td>".$row['LT_id']."</td>"; //Se puede usar el índice de la consulta
        $resultado .= "<td>".$row['L_nombre']."</td>"; //o el nombre de la columna
        $resultado .= "<td>".$row['T_nombre']."</td>";
        $resultado .= "<td>".$row['LT_Fecha']."</td>";
        $resultado .= "</tr>";
    }
    
    mysqli_free_result($resultados); //Liberar la memoria
  
    desconectar_bd($conexion_bd);   
      
    $resultado .= "</tbody></table>";
    return $resultado;
  }

  function insertarCaso($lugar, $tipo){
    $conexion_bd = conectar_bd();
    $dml_insertar = "CALL crearRegistro(?,?)";
    if(!($statement = $conexion_bd->prepare($dml_insertar)))
    {
      die("Error: (".$conexion_bd->errno.") ".$conexion_bd->error);
      return 0;
    }


    //unir parámetros de la función con la consulta
    //el primer arg es el formato de cada parámetro
    if(!$statement->bind_param("ii", $lugar, $tipo))
    {
      die("Error en vinculación: (".$statement->errno.") ".$statement->error);
      return 0;
    }

    //Ejecutar inserción
    if(!$statement->execute())
    {
      die("Error en ejecución: (".$statement->errno.") ".$statement->error);
      return 0;
    }

    desconectar_bd($conexion_bd);
    return 1;

  }
?>