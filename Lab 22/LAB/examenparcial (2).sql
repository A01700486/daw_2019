-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-05-2020 a las 03:44:46
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `examenparcial`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `crearRegistro` (IN `ulugar` INT(5), IN `utipo` INT(5))  NO SQL
INSERT INTO lugar_tipo (lugar,tipo)
VALUES (ulugar,utipo)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar_incidente`
--

CREATE TABLE `lugar_incidente` (
  `Id` int(5) NOT NULL,
  `Nombre_Lugar` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lugar_incidente`
--

INSERT INTO `lugar_incidente` (`Id`, `Nombre_Lugar`) VALUES
(1, 'Centro turístico'),
(2, 'Laboratorios'),
(3, 'Restaurante'),
(4, 'Centro operativo'),
(5, 'Triceratops'),
(6, 'Dilofosaurios'),
(7, 'Velociraptors'),
(8, 'TRex'),
(9, 'Planicie de los herbívoros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugar_tipo`
--

CREATE TABLE `lugar_tipo` (
  `Id` int(5) NOT NULL,
  `lugar` int(5) NOT NULL,
  `tipo` int(5) NOT NULL,
  `Fecha` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lugar_tipo`
--

INSERT INTO `lugar_tipo` (`Id`, `lugar`, `tipo`, `Fecha`) VALUES
(1, 1, 3, '2020-05-06 15:34:58'),
(2, 2, 5, '2020-05-06 15:56:37'),
(3, 1, 2, '2020-05-06 16:45:56'),
(4, 1, 4, '2020-05-06 16:55:51'),
(5, 1, 5, '2020-05-06 17:06:04'),
(6, 3, 2, '2020-05-06 17:14:39'),
(7, 3, 1, '2020-05-06 17:15:52'),
(8, 3, 4, '2020-05-06 17:16:00'),
(9, 3, 2, '2020-05-06 17:16:17'),
(10, 4, 1, '2020-05-06 17:18:46'),
(11, 6, 5, '2020-05-06 17:21:11'),
(12, 1, 1, '2020-05-06 17:21:33'),
(13, 2, 1, '2020-05-06 17:22:15'),
(14, 1, 1, '2020-05-30 01:06:16'),
(15, 6, 3, '2020-05-30 01:35:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_incidente`
--

CREATE TABLE `tipo_incidente` (
  `Id` int(5) NOT NULL,
  `NombreInc` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_incidente`
--

INSERT INTO `tipo_incidente` (`Id`, `NombreInc`) VALUES
(1, 'Falla eléctrica'),
(2, 'Fuga de herbívoro'),
(3, 'Fuga de Velociraptors'),
(4, 'Fuga de TRex'),
(5, 'Robo de ADN'),
(6, 'Auto descompuesto'),
(7, 'Visitantes en zona no autorizada');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `lugar_incidente`
--
ALTER TABLE `lugar_incidente`
  ADD PRIMARY KEY (`Id`);

--
-- Indices de la tabla `lugar_tipo`
--
ALTER TABLE `lugar_tipo`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `lugar` (`lugar`),
  ADD KEY `tipo` (`tipo`);

--
-- Indices de la tabla `tipo_incidente`
--
ALTER TABLE `tipo_incidente`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `lugar_incidente`
--
ALTER TABLE `lugar_incidente`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `lugar_tipo`
--
ALTER TABLE `lugar_tipo`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `tipo_incidente`
--
ALTER TABLE `tipo_incidente`
  MODIFY `Id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `lugar_tipo`
--
ALTER TABLE `lugar_tipo`
  ADD CONSTRAINT `lugar_tipo_ibfk_1` FOREIGN KEY (`tipo`) REFERENCES `tipo_incidente` (`Id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `lugar_tipo_ibfk_2` FOREIGN KEY (`lugar`) REFERENCES `lugar_incidente` (`Id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
