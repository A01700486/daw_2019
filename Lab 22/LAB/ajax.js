function registrarCasos(){
	let opcion = document.getElementById("tipo_incidente");
    let opcionVal = opcion.options[opcion.selectedIndex].value;
    let opcion2 = document.getElementById("lugar_incidente");
    let opcionVal2 = opcion2.options[opcion2.selectedIndex].value;
    let valor1=0;
    let valor2=0;
    if(opcionVal!="NA" && opcionVal2!="NA"){
        valor1 = opcionVal;
        valor2 = opcionVal2;
    }else{
        return 0;
    }

    $.post("controlador_registrar.php", {
        lugar: valor2,
        tipo: valor1
    }).done(function (data) {
        if(data==1){
            let ajaxResponse=document.getElementById('Mensaje');
            ajaxResponse.style.display="block";
            ajaxResponse.innerHTML = '<h3 align="center">Registro Exitoso</h3>';
            let select=document.getElementById('Select');
            select.style.display="none";
            let btn=document.getElementById('boton');
            btn.style.display="block";
        }else{
            let ajaxResponse=document.getElementById('Mensaje');
            ajaxResponse.style.display="block";
            ajaxResponse.innerHTML = '<h3 align="center">Registro No Exitoso</h3>';
            let select=document.getElementById('Select');
            select.style.display="none";
            let btn=document.getElementById('boton');
            btn.style.display="block";
        }
    });	
    mostrarCasos();
    mostrarCasos();
}

function mostrarCasos(){
	$.post("controlador_mostrar.php", {
     
    }).done(function (data) {
            let response=document.getElementById('Casos');
            response.style.display="block";
            let tabla=document.getElementById('Consulta');
            tabla.innerHTML = data;
    });
}

function regresarB(){
	let response=document.getElementById('Mensaje');
	response.style.display="none";
	let select=document.getElementById('Select');
	select.style.display="block";
	let casos=document.getElementById('Casos');
    casos.style.display="none";
    let btn=document.getElementById('boton');
    btn.style.display="none";
}



var habilitadaSeleccionarM=document.getElementById("RegistrarCaso");
if(habilitadaSeleccionarM!=null){
    habilitadaSeleccionarM.onclick = registrarCasos;
    var buscar=document.getElementById("VerCaso");
    buscar.onclick = mostrarCasos;
    var regresar=document.getElementById("regresar");
    regresar.onclick = regresarB;
}
