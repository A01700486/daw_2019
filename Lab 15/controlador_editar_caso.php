<?php
  session_start();
  require_once("model.php");  

  $_POST["caso"] = htmlspecialchars($_POST["caso"]);
  $_POST["estado"] = htmlspecialchars($_POST["estado"]);

  if(isset($_POST["caso"]) && isset($_POST["estado"])) {
      if (insertar_completo($_POST["caso"],$_POST["estado"])) {
          $_SESSION["mensaje"] = "Se editó el caso";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al editar el caso";
      }
  }

  header("location:index.php");
?>