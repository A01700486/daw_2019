<?php
  session_start();
  require_once("model.php");  

  $_POST["lugar"] = htmlspecialchars($_POST["lugar"]);
  $_POST["estado"] = htmlspecialchars($_POST["estado"]);

  if(isset($_POST["lugar"])) {
      if (insertar_caso($_POST["lugar"])) {
          $_SESSION["mensaje"] = "Se registró el caso";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al registrar el caso";
      }
  }
  if(isset($_POST["estado"])) {
    $id = consultar_idCaso();
    if (insertar_completo($id,$_POST["estado"])) {
          $_SESSION["mensaje"] = "Se registró el caso";
    } else {
          $_SESSION["warning"] = "Ocurrió un error al registrar el caso";
    }
  }

  header("location:index.php");
?>