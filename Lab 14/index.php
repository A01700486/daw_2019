<?php 
  require_once("model.php");  

  include("_header.html");  

  include("_form.html"); 

  if (isset($_POST["lugar"])) {
      $lugar = htmlspecialchars($_POST["lugar"]);
  } else {
      $lugar = "";
  }

if (isset($_POST["estado"])) {
      $estado = htmlspecialchars($_POST["estado"]);
  } else {
      $estado = "";
  }

if (isset($_POST["caso"])) {
      $fecha = htmlspecialchars($_POST["caso"]);
  } else {
      $fecha = "";
  }

  echo consultar_casos($lugar,$estado,$fecha);

  include("_footer.html"); 
?>