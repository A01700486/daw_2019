<?php 

  $promedio1=0;
  $promedio2=0;
  $mediana1=0;
  $mediana2=0;
  $arrayNormal=array(1,5,4,9,2,10);
  $arrayInverso=array();
  $arrayOrdenado=array();


  function problema1(){
    $numeros1 = array(24,7,1,45,10,13,17,37);
    $numeros2 = array(2,74,19,55,14,38,7,22);
    $aux = count($numeros1);
    $i = 0;
    global $promedio1, $promedio2;
    for($i; $i<$aux;$i++){
     $promedio1 = $promedio1 + $numeros1[$i];
     $promedio2 = $promedio2 + $numeros2[$i];
    }
    $promedio1 = $promedio1/count($numeros1);
    $promedio2 = $promedio2/count($numeros2);
  }

  function problema2(){
    global $mediana1, $mediana2;
    $numeros1 = array(24,7,1,45,10,13,17,37);
    $numeros2 = array(2,74,19,14,38,7,22);
    $aux1 = count($numeros1);
    $aux2 = count($numeros2);
    if($aux1 % 2 == 0){
      $mediana1 = ($numeros1[$aux1/2] + $numeros1[($aux1/2)-1]) / 2;
    }else{
      $mediana1 = $numeros1[floor($aux1/2)];
    }
    if($aux2 % 2 == 0){
      $mediana2 = ($numeros2[$aux2/2] + $numeros2[($aux2/2)-1]) / 2;
    }else{
      $mediana2 = $numeros2[floor($aux2/2)];
    }
  }


  function problema3May($x){
    $max = 0;
    $aux = 0;
    $aux2 = 0;
    for($i = 0; $i<count($x)-1; $i++){
      $max = $x[$i];
      $aux = $i;
      for($j = $i+1; $j<count($x); $j++ ){
        if($x[$j]>$max){
          $max=$x[$j];
          $aux=$j;
        }
      }
      $aux2 = $x[$aux];
      $x[$aux] = $x[$i];
      $x[$i] = $aux2;
      $aux2 = 0;
    }
    return $x;
  }

  function problemaMen($x){
    $min = 0;
    $aux = 0;
    $aux2 = 0;
    for($i = 0; $i<count($x)-1; $i++){
      $min = $x[$i];
      $aux = $i;
      for($j = $i+1; $j<count($x); $j++ ){
        if($x[$j]<$min){
          $min=$x[$j];
          $aux=$j;
        }
      }
      $aux2 = $x[$aux];
      $x[$aux] = $x[$i];
      $x[$i] = $aux2;
      $aux2 = 0;
    }
    return $x;
  }


  include("_header.html");
  problema1();
  problema2();  
  $arrayInverso =  problema3May($arrayNormal);
  $arrayOrdenado = problemaMen($arrayNormal);
  include("_body.html"); 

  include("_footer.html"); 
?> 