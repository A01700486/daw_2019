//Función para crear el objeto para realizar una petición asíncrona
function getRequestObject() {
    // Asynchronous objec created, handles browser DOM differences
    if (window.XMLHttpRequest) {
        // Mozilla, Opera, Safari, Chrome IE 7+
        return (new XMLHttpRequest());
    } else if (window.ActiveXObject) {
        // IE 6-
        return (new ActiveXObject("Microsoft.XMLHTTP"));
    } else {
        // Non AJAX browsers
        return (null);
    }
}


//Función que detonará la petición asíncrona
function buscar() {
    request = getRequestObject();
    if (request != null) {
        let lugar_id = document.getElementById("lugar").value;
        let estado_id = document.getElementById("estado").value;
        var url = 'controlador_buscar.php?lugar_id=' + lugar_id + '&estado_id=' + estado_id;
        
        request.open('GET', url, true);
        request.onreadystatechange =
            function () {
                if ((request.readyState == 4)) {
                    // Se recibió la respuesta asíncrona, entonces hay que actualizar el cliente.
                    // A esta parte comúnmente se le conoce como la función del callback
                    document.getElementById("resultados_consulta").innerHTML = request.responseText;
                }
            };
        // Limpiar la petición
        request.send(null);

    }
}

function sendRequest(){

   request=getRequestObject();
   if(request!=null)
   {
     let userInput = document.getElementById('userInput');
     let url='controlador_autocompletar.php?pattern='+userInput.value;
     request.open('GET',url,true);
     request.onreadystatechange = 
            function() { 
                if((request.readyState==4)){
                    // Asynchronous response has arrived
                    let ajaxResponse=document.getElementById('ajaxResponse');
                    ajaxResponse.innerHTML=request.responseText;
                    ajaxResponse.style.visibility="visible";
                }     
            };
     request.send(null);
   }
}

function selectValue() {

   let list=document.getElementById("list");
   let userInput=document.getElementById("userInput");
   let ajaxResponse=document.getElementById('ajaxResponse');
   userInput.value=list.options[list.selectedIndex].text;
   ajaxResponse.style.visibility="hidden";
   userInput.focus();
}

//Asignar al botón buscar, la función buscar()
document.getElementById("buscar").onclick = buscar;
document.getElementById("userInput").onkeyup=sendRequest;